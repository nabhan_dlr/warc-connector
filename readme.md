# Python Connector Prototype

### Install Requirements
+ Browse to this directory

By using pip 
+ Create virtualenv with: `python -m venv virtualenv`
+ Enter venv on linux `source virtualenv/bin/activate` or `.\virtualenv\Scripts\activate` if you are on windows
+ Install packages: `pip install -r requirements.txt`


### Start dev Server:
+ On the console type `uvicorn main:app` to start the server
+ This will start a debug server, you should see the following output:
  + `INFO:     Uvicorn running on http://127.0.0.1:8000 (Press CTRL+C to quit)`
+ Open http://127.0.0.1:8000/graphql to see GraphiQL UI
+ The connector endpoint is at http://127.0.0.1:8000/api/v0/connector/data

### Examples

<details>
  <summary>Start introspection</summary>

````
 query IntrospectionQuery {
    __schema {
      queryType { name }
      mutationType { name }
      subscriptionType { name }
      types {
        ...FullType
      }
      directives {
        name
        description
        locations
        args {
          ...InputValue
        }
        isRepeatable
      }
    }
  }

  fragment FullType on __Type {
    kind
    name
    description
    fields(includeDeprecated: true) {
      name
      description
      args {
        ...InputValue
      }
      type {
        ...TypeRef
      }
      isDeprecated
      deprecationReason
    }
    inputFields(includeDeprecated: true) {
      ...InputValue
    }
    interfaces {
      ...TypeRef
    }
    enumValues(includeDeprecated: true) {
      name
      description
      isDeprecated
      deprecationReason
    }
    possibleTypes {
      ...TypeRef
    }
  }

  fragment InputValue on __InputValue {
    name
    description
    type { ...TypeRef }
    defaultValue
    isDeprecated
    deprecationReason
  }

fragment TypeRef on __Type {
    kind
    name
    ofType {
      kind
      name
      ofType {
        kind
        name
        ofType {
          kind
          name
          ofType {
            kind
            name
            ofType {
              kind
              name
              ofType {
                kind
                name
                ofType {
                  kind
                  name
                }
              }
            }
          }
        }
      }
    }
  }
````
</details>

<details>
  <summary>Query Publications and Authors</summary>

````
query MyQuery1 {
  __typename
  ElibPublication(first:2000, offset:100) {
    uri
    title
    eprintid
    datestamp
    authors {
      family
      lineage
      honourific
      given
    }
    abstract
  }
}



query MyQuery2 {
  __typename
    ElibAuthor(first:50) {
      family
      lineage
      honourific
      given
    }

}
````


</details>
